package pl.codementors.notes;

import pl.codementors.notes.model.Note;//Note class is different package so it need to be imported.
import pl.codementors.notes.model.Notepad;

/**
 * Main application class simulating some user interaction.
 * @author psysiu
 */
public class NotesExamples {


    public static void main(String[] args) {

        Notepad notepad = new Notepad();

        Note note1 = new Note();
        note1.setTitle("Stuff to do:");
        note1.setContent("feed cat, feed dog, go out with dog");
        notepad.setNote(2, note1);

        Note note2 = new Note();
        note2.setTitle("Stuff to buy:");
        note2.setContent("dog food, cat food, my food (something cheap)");
        notepad.setNote(4, note2);


        Note fetchedNote = notepad.getNote(2);
        fetchedNote.printToOutput();

        notepad.printNotesToOutput();

    }
}
