package pl.codementors.notes.model;

/**
 * Represents some notepad which can store up to 20 notes.
 */
public class Notepad {

    private Note[] notes = new Note[20];

    public Note getNote(int index) {
        if (index >= 0 && index < notes.length) {
            return notes[index];
        } else {
            return null;
        }
    }

    public void setNote(int index, Note note) {
        if (index >= 0 && index < notes.length) {
            notes[index] = note;
        }
    }

    public void printNotesToOutput()
        {
            for (int i = 0; i < notes.length; i++) {
                if (notes[i] != null) {
                    System.out.println("--BEGIN NOTE--");
                    System.out.println("INDEX: " + i);
                    notes[i].printToOutput();
                    System.out.println("--END NOTE--");
                } else {
                    System.out.println("--BEGIN NOTE--");
                    System.out.println("INDEX: " + i);
                    System.out.println("--EMPTY--");
                    System.out.println("--END NOTE--");
                }
            }
        }
}
