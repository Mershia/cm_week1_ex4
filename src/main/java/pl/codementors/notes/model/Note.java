package pl.codementors.notes.model;

/**
 * Class representing single note. Note consists from title and content.
 * @author psysiu
 */
public class Note {


    private String title;
    private String content;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void printToOutput() {
        System.out.println("Title: " + getTitle());
        System.out.println("Content: " + getContent());
        System.out.println("--------*---------*--------");
    }
}
